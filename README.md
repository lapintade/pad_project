# DChat

DChat is a RESTful distributed chat using several technologies
  - JOnAS 5.3.0
  - JAX-RS Jersey 2.0
  - Java 2 EE
  - Google's Polymer
  - HTML5 / JavaScript

Basicaly you can connect to a chat room using

# Authors

Read contributors.txt for authors names

# Installation

EAR File will be provided for easier installation

# Structure

A DChat client contains 2 parts:
  - Servlet, used to contains many informations like Chat rooms, users, messages, etc.
  - Graphical Interface,used to communicate with other servlets thanks to GET/POST Request (defined by REST Structure).

# TODO's
  - Make this application working

# License

No licence has been defined, please stay tuned :)
