
-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'User'
-- 
-- ---

DROP TABLE IF EXISTS `User`;
		
CREATE TABLE `User` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `address` VARCHAR(64) NULL DEFAULT NULL,
  `username` VARCHAR(32) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Salon'
-- 
-- ---

DROP TABLE IF EXISTS `Salon`;
		
CREATE TABLE `Salon` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `salonname` VARCHAR(32) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Message'
-- 
-- ---

DROP TABLE IF EXISTS `Message`;
		
CREATE TABLE `Message` (
  `id` INTEGER NULL DEFAULT NULL,
  `content` MEDIUMTEXT NULL DEFAULT NULL,
  `id_Users` INTEGER NULL DEFAULT NULL,
  `id_Salons` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'userXsalon'
-- 
-- ---

DROP TABLE IF EXISTS `userXsalon`;
		
CREATE TABLE `userXsalon` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `id_User` INTEGER NULL DEFAULT NULL,
  `id_Salon` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `Message` ADD FOREIGN KEY (id_Users) REFERENCES `User` (`id`);
ALTER TABLE `Message` ADD FOREIGN KEY (id_Salons) REFERENCES `Salon` (`id`);
ALTER TABLE `userXsalon` ADD FOREIGN KEY (id_User) REFERENCES `User` (`id`);
ALTER TABLE `userXsalon` ADD FOREIGN KEY (id_Salon) REFERENCES `Salon` (`id`);

-- ---
-- Table Properties
-- ---

ALTER TABLE `User` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `Salon` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `Message` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `userXsalon` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

INSERT INTO `User` (`id`,`address`,`username`) VALUES ('','http://localhost:9000/PAD_Project ','Nicolas ROZE');
INSERT INTO `User` (`id`,`address`,`username`) VALUES ('','http://localhost:9000/PAD_Project ','Ivan Sivric');
INSERT INTO `User` (`id`,`address`,`username`) VALUES ('','http://localhost:9000/PAD_Project ','Bob BLOB');

INSERT INTO `Salon` (`id`,`salonname`) VALUES ('','Premier Salon');
INSERT INTO `Salon` (`id`,`salonname`) VALUES ('','Deuxieme Salon');
INSERT INTO `Salon` (`id`,`salonname`) VALUES ('','Anonymous');

-- INSERT INTO `Message` (`id`,`content`,`id_Users`,`id_Salons`) VALUES ('','','','');

INSERT INTO `userXsalon` (`id`,`id_User`,`id_Salon`) VALUES ('','1','1');
INSERT INTO `userXsalon` (`id`,`id_User`,`id_Salon`) VALUES ('','2','1');
INSERT INTO `userXsalon` (`id`,`id_User`,`id_Salon`) VALUES ('','3','1');

INSERT INTO `userXsalon` (`id`,`id_User`,`id_Salon`) VALUES ('','1','2');
INSERT INTO `userXsalon` (`id`,`id_User`,`id_Salon`) VALUES ('','2','2');

INSERT INTO `userXsalon` (`id`,`id_User`,`id_Salon`) VALUES ('','1','3');

