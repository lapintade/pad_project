package client.servlet;

import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import model.Message;
import model.Salon;
import model.User;
import model.UserXsalon;
import server.SalonManagerRemote;

@Path("/salon")
public class SalonRessource {
	private static final String HOST = "http://localhost:9000/PAD_Project/DChatApp/";
	private SalonManagerRemote smb;
	public SalonRessource() {
		Hashtable<String, String> h = new Hashtable<String, String>();	
		h.put("java.naming.factory.initial","org.objectweb.carol.jndi.spi.MultiOrbInitialContextFactory");	
		h.put("java.naming.provider.url","rmi://localhost:1099");
		h.put("java.naming.factory.url.pkgs", "org.objectweb.jonas.naming");
		Context ctx;
		
		try {
			ctx = new InitialContext(h);
			String JNDI_NAME = "SMB";
			smb = (SalonManagerRemote) ctx.lookup(JNDI_NAME);
			
			//JNDI_NAME = "UMB";
			//umb = (UserManagerRemote) ctx.lookup(JNDI_NAME);
		} catch (NamingException e) {
			System.out.println("Exception: " + e);
			//e.printStackTrace();
		}
		//smb = new SalonManagerBean();
		//System.out.println("SalonRessource()");
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response jsonGetSalonList() {
		// TODO Get a list of salon nom and id for connexion page
		String temp = "{\"host\":\"" + HOST + "\",\"salon\":";
		String salon = "[";
		for(Salon s: smb.retrieve()) {
			int nbUser =  smb.getUserBySalonId(s.getId()).size();
			salon += "{\"nom\":\"" + s.getSalonname() + "\",\"nbUser\":" + nbUser + "},";
		}
		salon = salon.substring(0, salon.length() - 1);
		salon += "]";
		temp += salon + "}";
		
		return Response
	            .status(200)
	            .header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
	            .entity(temp)
	            .build();
	}
	
	@Path("/{id}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response jsonGetSalonID(@PathParam("id") int id) {
		Salon currentSalon = smb.searchSalon(id);
		List<User> lu = smb.getUserBySalonId(id);
		String salon_name = currentSalon.getSalonname();
		// TODO Register User to salon and server gives salon informations
		String temp = "{\"nom\":\""+salon_name+"\",\"id\":"+currentSalon.getId()+",\"users\":[";
		temp += getStringUsers(lu) + "]}";
		return makeResponse(temp, 200);
	}
	
	@Path("/connect")
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public Response jsonConnexionToSalon(@FormParam("salon_id") int salon, @FormParam("username") String username) {
		// enregistrer l'utilisateur dans le salon
		//smb.registerUserToSalonId(username, "TEST", salon);
		Salon s = smb.searchSalon(salon);
		User u = smb.returnUserOrPersist(username, HOST);
		System.out.println("Connexion de " + u.getUsername() + " id " + u.getId());
		
		if(smb.registerUserToSalonId(u,s))
			return makeResponse("OK", 200);
		else
			return makeResponse("FAIL", 403);
	}
	
	public Response makeResponse(String s, int code) {
		return Response
	            .status(code)
	            .header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
	            .entity(s)
	            .build();
	}
	
	public String getStringUsers(List<User> lu) {
		String ret = "";
		
		if(lu.size() != 0) {
			for(User u: lu) {
				ret += "{\"username\":\"" + u.getUsername()+ "\", \"id\":" + u.getId() + ", \"address\":\""+u.getAddress()+"\"},";
			}
			ret = ret.substring(0, ret.length() - 1);
		}
		return ret;
	}
	
	@POST
	@Path("/disconnect/{username}")
	public Response disconnection(@PathParam("username") String username) {
		// faire requete pour remove user dans userXsalon
		
		System.out.println("Deconnection de " + username);
		smb.disconnectUsernameToSalon(username);
		return makeResponse("TEST", 200);
	}
	
	// La méthode GET devrait retrouner tout les messages de l'utilisateur dans le salon
	@Path("/{id}/{username}")
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	public Response addMessageToSalon(@PathParam("id") int id, @PathParam("username") String username, String message) {
		// faire requete pour récuprer
		Salon s = smb.searchSalon(id);
		User u = smb.returnUserOrPersist(username, HOST);
		smb.postMessage(u, s, message);
		return makeResponse("OK", 200);
	}
	
	@Path("/history/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessageHistory(@PathParam("id") int id) {
		String tmp = "[";
		List<Message> messages = smb.getSalonHistory(id);
		for(Message m: messages) {
			tmp += "{\"id\":" +m.getId() + ",\"content\":\"" +m.getContent() + "\",\"username\":\"" +m.getUser().getUsername() + "\",\"date_envoi\":\"" +m.getDateEnvoi() + "\"},";
		}
		tmp = tmp.substring(0, tmp.length() - 1);
		tmp += "]";
		return makeResponse(tmp, 200);
	}
}
