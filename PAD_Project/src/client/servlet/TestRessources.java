package client.servlet;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/test")
public class TestRessources {
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String plainTextHelloWorld() {
		return "Hello World world";
	}
	
	@GET
	@Produces(MediaType.TEXT_XML)
	public String xmlHelloWorld() {
		 return "<?xml version=\"1.0\"?>" + "<hello> Hello World world" + "</hello>";
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String htmlHellowWorld() {
		return "<html> " + "<title>" + "Hello World html world" + "</title>"
		        + "<body><h1>" + "Hello World world html" + "</body></h1>" + "</html> ";
	}
}
