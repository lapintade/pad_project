<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <title>Chat</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/ripples.min.css" rel="stylesheet">
        <link href="css/material-wfont.min.css" rel="stylesheet">
        <link href="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.css" rel="stylesheet">
        <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="js/ajax.js"></script>
    </head>
    <body onLoad="loadChat()">
         <input type="hidden" value="<?php echo $_GET["username"] ?>" id="username"/>
          <input type="hidden" value="<?php echo $_GET["salon_id"] ?>" id="salon_id"/>
        <script src="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/ripples.min.js"></script>
        <script src="js/material.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/6.2.0/jquery.nouislider.min.js"></script>
        <div class="container">
            <div class="row " >
                <ul class="nav nav-tabs col-md-12" style="margin-bottom: 15px;">
                    <li class="active"><a href="#home" data-toggle="tab" onclick="javascript:$.snackbar({content: 'Vous avez changé de salon'});" id="tabSalonName">Salon 1</a></li>
                    <li><a href="#profile1" data-toggle="tab" onclick="javascript:$.snackbar({content: 'Vous avez changé de salon'});">404</a></li>
                     <li><a href="#none" data-toggle="tab">+</a></li>
                     <li style="float:right"><a href="#none" data-toggle="tab" onclick="javascript:logOut();">Deconnexion</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="home">
                        <div class="col-md-8">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    CONVERSATION
                                </div>
                                <div id="messageContainer" class="panel-body" style="overflow:scroll; height:500px;overflow-x:hidden;">
                                    <ul class="media-list">
                                        <!-- CONVERSATION -->
                                    </ul>
                                </div>
                                <div class="panel-footer">
                                    <div class="input-group">
                                        <input id="messageInput" type="text" class="form-control" placeholder="Entrer un message" />
                                        <span class="input-group-btn">
                                        <button id="sendMessage" class="btn btn-primary" type="button">ENVOYER</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile1">
                        <div class="col-md-8">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    CONVERSATION
                                </div>
                                <div class="panel-body">
                                    <ul class="media-list">
                                        <!-- CONVERSATION -->
                                    </ul>
                                </div>
                                <div class="panel-footer">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Entrer un message" />
                                        <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">ENVOYER</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            UTILISATEURS EN LIGNE
                        </div>
                        <div class="panel-body">
                            <ul class="media-list" id="listUser">
                                <!-- USERS -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
