$(document).ready(function() {
    HOST = "http://localHOST:9000/PAD_Project/DChatApp/";
    
    $("#sendMessage").click(function() {
        sendMessage();
    });
    
    
    $(window).bind('beforeunload', function(){ 
        logOut();
    });
});


function getAndDisplaySalons() {
    $.getJSON( "http://localHOST:9000/PAD_Project/DChatApp/salon/", function( salonJSON ) {
        $.each(salonJSON.salon, function(index, value) {
            // TODO Checking if salons still exists before displaying
            //console.log(index, value.nom, value.nbUser);
            $("select#salon").append( new Option(value.nom + "(" + value.nbUser + ")", index));
        });
    });
    
}

function connexionToSalon() {
    var id = parseInt($("#salon option:selected").val()) + 1;
    var username = $("#inputTextPseudo").val();
    var datastr = "salon_id=" + id + "&username=" + username;

    $.ajax({
        type: "POST",
        url: HOST + "salon/connect",
        data: datastr,
        success: function(data, textStatus, jqXHR) {
            console.log("SUCCES");
            window.location = "http://localhost:8080/chat.php?salon_id=" + id + "&username=" + username;
        },
        statusCode: {
            403: function(xhr) {
                alert("Utilisateur deja dans le salon");
            }
        }
    });
    
}

function logOut() {
    var username = $("#username").val();
    if(typeof username !== "undefined") {
        $.ajax({
            type: "POST",
            url: HOST + "salon/disconnect/" + username,
            success: function(data, textStatus, jqXHR) {
                console.log("DISCONNECTED");
                window.location = "http://localhost:8080/connexion.html";
            }
        });
    }
}

function loadChat() {
    
    
    loadUser();
    loadMessage();
    refreshAll();
}

function loadUser() {
    var id = $("#salon_id").val();
    $.ajax({
        type: "POST",
        url: HOST + "salon/" + id,
        success: function(data, textStatus, jqXHR) {
            $("#listUser").empty();
            for(var i=0; i < data.users.length; i++) {
                var temp = ' <li class="media"><div class="media-body"><div class="media"><a class="pull-left" href="#"><img class="media-object img-circle" style="max-height:40px;" src="img/nico.png" /></a><div class="media-body" ><h5>' + data.users[i].username + '</h5><small class="text-muted">Connecté depuis X heures</small></div></div></div></li>';
                $("#listUser").append(temp);
            }
        }
    });
}

function refreshAll() {
    setInterval("loadUser()", 1000);
    setInterval("loadMessage()", 1000)
}

function loadMessage() {
    var id = $("#salon_id").val();
    
    $.ajax({
        type: "GET",
        url: HOST + "salon/history/" + id,
        success: function(data, textStatus, jqXHR) {
            $("#home ul").empty();
            for(var i=0; i < data.length; i++) {
                //console.log(data[i].content);
                $("#home ul").append(htmlMessage(data[i].content, data[i].username, data[i].date_envoi));
            }
            $("#messageContainer").scrollTop($("#messageContainer")[0].scrollHeight);
        }
    });
}

function htmlMessage(content, username, date) {
    return "<li class='media'><div class='media-body'><div class='media'><a class='pull-left' href='#'><img class='media-object img-circle ' style='max-height:60px;' src='img/nico.png' /></a><div class='media-body' > " + content + "<br /><small class='text-muted'>"+username+" @ " + date + "</small><hr /></div></div></div></li>";
}

function sendMessage() {

    var username = $("#username").val();
    var content = $("#messageInput").val();
    // 2015-01-29 02:30:13
    var d = new Date();
    var strDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
    var message = htmlMessage(content, username, strDate);
    $("#home ul").append(message);

    $(".panel-body").animate({ scrollTop: $("#myDiv").attr("scrollHeight") }, 3000);
    var id = $("#salon_id").val();
    var username = $("#username").val();
    $.ajax({
        type: "POST",
        url: HOST + "salon/" + id + "/" + username,
        data: content,
        contentType: false,
        success: function(data, textStatus, jqXHR) {
           $("#messageInput").empty();
           $("#messageContainer").scrollTop($("#messageContainer")[0].scrollHeight);
        }
    });
    
    
}

function addSalon() {
    console.log("ADD Salon");
}

function getCurrentDate() {
    var d = new Date();
    var dd = d.getDate();
    var mm = d.getMonth() + 1;
    var yyyy = d.getFullYear();
    
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
    return yyyy + '-' + mm + '-' + dd;
}
