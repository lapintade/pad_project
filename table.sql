-- phpMyAdmin SQL Dump
-- version 4.2.12deb1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 29 Janvier 2015 à 03:51
-- Version du serveur :  10.0.15-MariaDB-3
-- Version de PHP :  5.6.4-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `DChat`
--

-- --------------------------------------------------------

--
-- Structure de la table `Message`
--

CREATE TABLE IF NOT EXISTS `Message` (
`id` int(11) NOT NULL,
  `content` mediumtext CHARACTER SET latin1,
  `id_Users` int(11) DEFAULT NULL,
  `id_Salons` int(11) DEFAULT NULL,
  `date_envoi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `Message`
--

INSERT INTO `Message` (`id`, `content`, `id_Users`, `id_Salons`, `date_envoi`) VALUES
(1, 'Coucou', 8, 1, '2015-01-29 01:50:50'),
(2, 'Ca va ?', 8, 1, '2015-01-29 01:50:57'),
(3, 'azerty', 8, 1, '2015-01-29 01:53:21'),
(4, 'ytreza', 8, 1, '2015-01-29 02:04:24'),
(5, 'qsdf', 8, 1, '2015-01-29 02:25:22'),
(6, 'Bob is life', 8, 1, '2015-01-29 02:25:37'),
(7, 'Je suis Bob', 8, 1, '2015-01-29 02:27:58'),
(8, '*bump*', 5, 1, '2015-01-29 02:36:44'),
(9, 'izi', 5, 1, '2015-01-29 02:40:13'),
(10, 'Tranquil ?', 8, 1, '2015-01-29 02:40:38'),
(11, 'Bien et toi ?', 8, 1, '2015-01-29 02:41:20'),
(13, 'z', 5, 1, '2015-01-29 02:45:15'),
(14, 'zer', 8, 1, '2015-01-29 02:45:21'),
(15, 'zeaze', 5, 1, '2015-01-29 02:45:48'),
(16, 'Hello !', 5, 1, '2015-01-29 02:48:34'),
(17, 'PAD PAD PAD !', 5, 1, '2015-01-29 02:49:13');

-- --------------------------------------------------------

--
-- Structure de la table `Salon`
--

CREATE TABLE IF NOT EXISTS `Salon` (
`id` int(11) NOT NULL,
  `salonname` varchar(32) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `Salon`
--

INSERT INTO `Salon` (`id`, `salonname`) VALUES
(1, 'Premier Salon'),
(2, 'Deuxieme Salon'),
(3, 'TEST');

-- --------------------------------------------------------

--
-- Structure de la table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
`id` int(11) NOT NULL,
  `address` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `username` varchar(32) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `User`
--

INSERT INTO `User` (`id`, `address`, `username`) VALUES
(1, 'http://localhost:9000/PAD_Project ', 'Nicolas ROZE'),
(2, 'http://localhost:9000/PAD_Project ', 'Ivan Sivric'),
(3, 'http://localhost:9000/PAD_Project ', 'Bob BLOB'),
(5, 'http://localhost:9000/PAD_Project/DChatApp/', 'LapinTade'),
(7, 'http://localhost:9000/PAD_Project/DChatApp/', 'Nicolas'),
(8, 'http://localhost:9000/PAD_Project/DChatApp/', 'bob');

-- --------------------------------------------------------

--
-- Structure de la table `userXsalon`
--

CREATE TABLE IF NOT EXISTS `userXsalon` (
`id` int(11) NOT NULL,
  `id_User` int(11) DEFAULT NULL,
  `id_Salon` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `userXsalon`
--

INSERT INTO `userXsalon` (`id`, `id_User`, `id_Salon`) VALUES
(4, 1, 2),
(5, 2, 2);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Message`
--
ALTER TABLE `Message`
 ADD PRIMARY KEY (`id`), ADD KEY `id_Users` (`id_Users`), ADD KEY `id_Salons` (`id_Salons`);

--
-- Index pour la table `Salon`
--
ALTER TABLE `Salon`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `User`
--
ALTER TABLE `User`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `userXsalon`
--
ALTER TABLE `userXsalon`
 ADD PRIMARY KEY (`id`), ADD KEY `id_User` (`id_User`), ADD KEY `id_Salon` (`id_Salon`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Message`
--
ALTER TABLE `Message`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `Salon`
--
ALTER TABLE `Salon`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `User`
--
ALTER TABLE `User`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `userXsalon`
--
ALTER TABLE `userXsalon`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Message`
--
ALTER TABLE `Message`
ADD CONSTRAINT `Message_ibfk_1` FOREIGN KEY (`id_Users`) REFERENCES `User` (`id`),
ADD CONSTRAINT `Message_ibfk_2` FOREIGN KEY (`id_Salons`) REFERENCES `Salon` (`id`);

--
-- Contraintes pour la table `userXsalon`
--
ALTER TABLE `userXsalon`
ADD CONSTRAINT `userXsalon_ibfk_1` FOREIGN KEY (`id_User`) REFERENCES `User` (`id`),
ADD CONSTRAINT `userXsalon_ibfk_2` FOREIGN KEY (`id_Salon`) REFERENCES `Salon` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

