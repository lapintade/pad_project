package model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the userXsalon database table.
 * 
 */
@Entity
@Table(name="userXsalon")
@NamedQueries({
	@NamedQuery(name="UserXsalon.findAll", query="SELECT u FROM UserXsalon u")
})
public class UserXsalon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="id_User")
	private User user;

	//bi-directional many-to-one association to Salon
	@ManyToOne
	@JoinColumn(name="id_Salon")
	private Salon salon;

	public UserXsalon() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Salon getSalon() {
		return this.salon;
	}

	public void setSalon(Salon salon) {
		this.salon = salon;
	}

}