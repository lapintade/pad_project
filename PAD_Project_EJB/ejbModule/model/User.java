package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the User database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="User.findAll", query="SELECT u FROM User u"),
	@NamedQuery(name="User.findUsername", query="SELECT u FROM User u WHERE u.username= :username")
})

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String address;

	private String username;

	//bi-directional many-to-one association to Message
	@OneToMany(mappedBy="user")
	private List<Message> messages;

	//bi-directional many-to-one association to UserXsalon
	@OneToMany(mappedBy="user")
	private List<UserXsalon> userXsalons;

	public User() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Message addMessage(Message message) {
		getMessages().add(message);
		message.setUser(this);

		return message;
	}

	public Message removeMessage(Message message) {
		getMessages().remove(message);
		message.setUser(null);

		return message;
	}

	public List<UserXsalon> getUserXsalons() {
		return this.userXsalons;
	}

	public void setUserXsalons(List<UserXsalon> userXsalons) {
		this.userXsalons = userXsalons;
	}

	public UserXsalon addUserXsalon(UserXsalon userXsalon) {
		getUserXsalons().add(userXsalon);
		userXsalon.setUser(this);

		return userXsalon;
	}

	public UserXsalon removeUserXsalon(UserXsalon userXsalon) {
		getUserXsalons().remove(userXsalon);
		userXsalon.setUser(null);

		return userXsalon;
	}

}