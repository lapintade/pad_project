package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Salon database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Salon.findAll", query="SELECT s FROM Salon s"),
	@NamedQuery(name="Salon.findById", query="SELECT s FROM Salon s WHERE s.id = :id"),
	@NamedQuery(name="Salon.getNbUser", query="SELECT u FROM User u, UserXsalon x WHERE u.id = x.user.id AND x.salon.id=:id")
})
public class Salon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String salonname;

	//bi-directional many-to-one association to Message
	@OneToMany(mappedBy="salon")
	private List<Message> messages;

	//bi-directional many-to-one association to UserXsalon
	@OneToMany(mappedBy="salon")
	private List<UserXsalon> userXsalons;

	public Salon() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSalonname() {
		return this.salonname;
	}

	public void setSalonname(String salonname) {
		this.salonname = salonname;
	}

	public List<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Message addMessage(Message message) {
		getMessages().add(message);
		message.setSalon(this);

		return message;
	}

	public Message removeMessage(Message message) {
		getMessages().remove(message);
		message.setSalon(null);

		return message;
	}

	public List<UserXsalon> getUserXsalons() {
		return this.userXsalons;
	}

	public void setUserXsalons(List<UserXsalon> userXsalons) {
		this.userXsalons = userXsalons;
	}

	public UserXsalon addUserXsalon(UserXsalon userXsalon) {
		getUserXsalons().add(userXsalon);
		userXsalon.setSalon(this);

		return userXsalon;
	}

	public UserXsalon removeUserXsalon(UserXsalon userXsalon) {
		getUserXsalons().remove(userXsalon);
		userXsalon.setSalon(null);

		return userXsalon;
	}

}