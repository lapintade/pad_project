package server;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import model.Message;
import model.Salon;
import model.User;
import model.UserXsalon;

@Stateless(name="SalonManager" , mappedName="SMB")
public class SalonManagerBean implements SalonManagerRemote {
	@PersistenceContext
	private EntityManager em;

	@Override
	public void createSalon(String salonname) {
		Salon s = new Salon();
		s.setSalonname(salonname);
		this.em.persist(s);
		em.flush();
	}

	@Override
	public Salon searchSalon(int id) {
		Salon s = em.find(Salon.class, id);
		return s;
	}

	@Override
	public void deleteSalon(int id) {
		Salon s = em.find(Salon.class, id);
		em.remove(s);
	}

	@Override
	public void setSalonSalonname(int id, String salonname) {
		Salon s = em.find(Salon.class, id);
		s.setSalonname(salonname);
		em.flush();
	}

	@Override
	public List<Salon> retrieve() {
		Query q = em.createNamedQuery("Salon.findAll");
		return q.getResultList();
	}
	
	public List<User> getUserBySalonId(int id) {
		Query q = em.createNamedQuery("Salon.getNbUser");
		q.setParameter("id", id);
		return q.getResultList();
	}
	
	@Override
	public boolean registerUserToSalonId(User u, Salon s) {
		UserXsalon uxs = new UserXsalon();
		uxs.setUser(u);
		uxs.setSalon(s);
		List<UserXsalon> lu = em.createNamedQuery("UserXsalon.findAll").getResultList();
		for(UserXsalon tmp: lu) {
			if(tmp.getUser().getId() == u.getId() && tmp.getSalon().getId() == s.getId()) {
				System.out.println("User already in salon");
				return false;
			}
		}
		em.persist(uxs);
		em.flush();
		return true;
	}
	
	
	@Override
	public void disconnectUsernameToSalon(String username) {
		Query q = em.createNamedQuery("User.findUsername");
		q.setParameter("username", username);
		try {
			User u = (User) q.getSingleResult();
			
			List<UserXsalon> lu = em.createNamedQuery("UserXsalon.findAll").getResultList();
			for(UserXsalon tmp: lu) {
				if(tmp.getUser().getId() == u.getId()) {
					em.remove(tmp);
					em.flush();
					System.out.println("Removing " + username + " from Salon " + tmp.getSalon().getId());;
				}
			}
		} catch(NoResultException e) {
			System.out.println("No user know as " + username);
		}
			
	}
	
	
	@Override
	public User returnUserOrPersist(String username, String address) {
		Query q = em.createNamedQuery("User.findUsername");
		q.setParameter("username", username);
		try {
			User u = (User) q.getSingleResult();
			em.refresh(u);
			return u;
		} catch(NoResultException e) {
			User u = new User();
			u.setUsername(username);
			u.setAddress(address);
			em.persist(u);
			em.flush();
			
			u = (User) q.getSingleResult();
			return u;
		}
	}
	
	@Override
	public void postMessage(User u, Salon s, String message) {
		Message m = new Message();
		m.setSalon(s);
		m.setUser(u);
		m.setContent(message);
		em.persist(m);
		em.flush();
	}
	
	@Override
	public List<Message> getSalonHistory(int id) {
		Query q = em.createNamedQuery("Message.findAll");
		List<Message> ret = q.getResultList();
		for(Message m: ret) {
			if(m.getSalon().getId() != id) {
				ret.remove(m);
			}
		}
		return ret;
	}
}
