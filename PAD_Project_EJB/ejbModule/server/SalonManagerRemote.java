package server;

import java.util.List;

import javax.ejb.Remote;

import model.Message;
import model.Salon;
import model.User;

/**
 * Session Bean implementation class SalonManager
 */
@Remote
public interface SalonManagerRemote{
	public void createSalon(String salonname);
	public Salon searchSalon(int id);
	public void deleteSalon(int id);
	public void setSalonSalonname(int id, String salonname);
	public List<Salon> retrieve();
	public List<User> getUserBySalonId(int id);
	public boolean registerUserToSalonId(User u, Salon s);
	public User returnUserOrPersist(String username, String address);
	public void disconnectUsernameToSalon(String username);
	public void postMessage(User u, Salon s, String message);
	public List<Message> getSalonHistory(int i);
}
